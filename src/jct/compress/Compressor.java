package jct.compress;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class can be used to compress arbitrary bytes by using external
 * compression programs. The class cannot be initialized by itself, use
 * {@link CompressionBuilder} instead.
 * 
 *
 */
public class Compressor {
	/*
	 * This lock is claimed by the CompressorInputReader when it is started as a
	 * thread. This makes sure that the reading of subprocess input is finished
	 * before the data can be accessed.
	 */
	private Lock processWait = new ReentrantLock();

	/*
	 * Used to halt the constructor of the Compressor, until the
	 * CompressorInputReader is ready has picked up the Lock
	 */
	private CountDownLatch processStart = new CountDownLatch(1);

	/*
	 * Subprocess, the actual compresser program
	 */
	private Process proc;

	/*
	 * InputStream to read the output of the process
	 */
	private InputStream processOutput;

	/*
	 * The outputstream that is connected to process input, to put the data into.
	 */
	private OutputStream processInput;

	/*
	 * Max size for the output byte array.
	 */
	private long maxSize = Integer.MAX_VALUE - 10;

	/*
	 * The reader for the process output. Reads and collects it to an InputStream.
	 */
	private CompressorInputReader in;

	protected Compressor(Algorithm program, String[]... args) throws IOException, InterruptedException{
		ProcessBuilder pb = new ProcessBuilder();
		ArrayList<String> arguments = new ArrayList<>();
		arguments.add(program.programName);
		for (String[] a : args){
			arguments.addAll(Arrays.asList(a));
		}
		proc = pb.command(arguments).start();
		processOutput = new BufferedInputStream(proc.getInputStream());
		processInput = proc.getOutputStream();

		in = new CompressorInputReader(processOutput);
		new Thread(in, "jCompress input reader").start();
		processStart.await();
	}

	/**
	 * Defines a maximum size for read data that remains in the memory. If more
	 * bytes are read, a temporary file is created.
	 * 
	 * @param size how many bytes can be read before using a temp file
	 */
	private void setMaxSize(int size){
		this.maxSize = size;
	}

	/**
	 * Reads the contents of the file into an array. Fails if the file is larger
	 * than array can take. This method blocks until the compressor program is done.
	 * 
	 * @return a byte array that contains the compressed/decompressed data
	 */
	public byte[] getBytes(){
		return in.readFully();
	}

	/**
	 * Returns an inputstream that contains the compressed byte array, or a
	 * temporary file. This method blocks until the compressor program is done.
	 * 
	 * @return an InputStream that contains the compressed/decompressed data
	 */
	public InputStream getInputStream(){
		return getInputStream();
	}

	/**
	 * Returns the size of the compressed data.
	 * 
	 * @return size of the compressed data
	 */
	public long getCompressedLength(){
		return in.getTotalRead();
	}

	/**
	 * Write more bytes to the subprocess
	 * 
	 * @param bytes a byte array that will be written to the compression program
	 * @throws IOException if there is any I/O error while writing data to the
	 *                     subprocess
	 */
	public void write(byte[] bytes) throws IOException{
		processInput.write(bytes);
	}

	/**
	 * Finished compressing. After this, no more bytes can be written. This must be
	 * done to guarantee that the subprocess will eventually finish.
	 * 
	 * @throws IOException if there is any I/O error while writing data to the
	 *                     subprocess
	 */
	public void finish() throws IOException{
		processInput.flush();
		processInput.close();
	}

	/**
	 * 
	 * A runnable that will read the subprocess stream asynchronously.
	 *
	 */
	private class CompressorInputReader implements Runnable {

		/*
		 * The actual InputStream that will be read
		 */
		private InputStream in;

		/*
		 * The output where results will be written
		 */
		private OutputStream out = new ByteArrayOutputStream();

		/*
		 * Marks the fact that temp file now exists
		 */
		private boolean tempFile;

		/*
		 * The temp while, where data might be written, if the max size is reached.
		 */
		private File tmp;

		/*
		 * How many bytes have been read from the subprocess
		 */
		private long written;

		/**
		 * Creates a CompressorInputReader for given stream. The only user will be the
		 * enclosing class.
		 * 
		 * @param in
		 */
		private CompressorInputReader(InputStream in){
			this.in = in;
		}

		/**
		 * Returns how many bytes have been read back from the subprocess. The
		 * subprocess might not write anything before it has read the input fully.
		 * 
		 * @return
		 */
		public long getTotalRead(){
			return written;
		}

		/**
		 * f Reads the InputStream completely. If the InputStream is still a
		 * ByteArrayOutputStream, the contents of that are returned. Otherwise the
		 * actual InputStream will be read.
		 * 
		 * @return
		 */
		public byte[] readFully(){
			try{
				processWait.lock();
				if (out instanceof ByteArrayOutputStream){
					return ((ByteArrayOutputStream) out).toByteArray();
				}
				ByteArrayOutputStream b = new ByteArrayOutputStream();
				try{

					InputStream is = getInputStream();
					int read = 0;
					byte[] buffer = new byte[1024];
					long written = 0;

					while ((read = is.read(buffer)) != -1){
						checkCapacity(written + read);
						b.write(buffer, 0, read);
						written += read;
					}

					processOutput.close();
				}catch(IOException e){
					throw new RuntimeException(e);
				}
				return b.toByteArray();

			}finally{
				processWait.unlock();
			}
		}

		/**
		 * Returns the InputStream for the data that was read from the subprocess.
		 * 
		 * @return the InputStream that contains the read data.
		 */
		public InputStream getInputStream(){
			try{
				processWait.lock();
				if (tempFile){
					try{
						return new FileDeletingInputStream(tmp);
					}catch(FileNotFoundException e){
						throw new RuntimeException(e);
					}
				}

				return new ByteArrayInputStream(((ByteArrayOutputStream) out).toByteArray());
			}finally{
				processWait.unlock();
			}
		}

		@Override
		public void run(){
			try{
				processWait.lock();
				processStart.countDown();
				byte[] buffer = new byte[1024];
				written = 0;
				int read = 0;
				try{
					while ((read = in.read(buffer)) != -1){
						checkCapacity(written + read);
						out.write(buffer, 0, read);
						written += read;
					}
					processOutput.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}finally{
				processWait.unlock();
			}
		}

		/**
		 * If the max size of the output is reached, contents are dumped into an empty
		 * file.
		 * 
		 * @param total
		 * @throws IOException
		 */
		public void checkCapacity(long total) throws IOException{
			if (maxSize == 0){
				return;
			}
			if (!tempFile && total > maxSize){
				File f = File.createTempFile("java-compression.", ".tmp");
				tempFile = true;
				this.tmp = f;
				FileOutputStream fos = new FileOutputStream(f);
				fos.write(((ByteArrayOutputStream) out).toByteArray());
				out.close();
				out = fos;

			}
		}
	}

	/**
	 * A FileInputStream, that deletes the file after reading. Be careful where to
	 * use. Because temp files might be created a lot, they could clutter the temp
	 * folder. This makes sure the file is always deleted after reading.
	 *
	 */
	private class FileDeletingInputStream extends FileInputStream {

		private File tempfile;

		public FileDeletingInputStream(File file) throws FileNotFoundException{
			super(file);
			this.tempfile = file;
		}

		@Override
		public void close() throws IOException{
			super.close();
			tempfile.delete();
		}
	}

	/**
	 * 
	 * Used to build the compressor/decompressor object.
	 *
	 */
	public static class CompressionBuilder {
		public static int ARRAY_MAX_SIZE = Integer.MAX_VALUE - 8;

		private String[] defaultDecompressArgs = new String[0];
		private String[] args = new String[0];
		private int maxSize = ARRAY_MAX_SIZE;
		private Algorithm prog;

		/**
		 * Returns a builder for the compression algorithm. This is a blank template and
		 * can be customized with any way by giving arguments with
		 * {@link CompressionBuilder#args(String... args)}
		 * 
		 * @param prog an {@link Algorithm} to be used in compression
		 * @return this
		 */
		public static CompressionBuilder compress(Algorithm prog){
			return new CompressionBuilder(prog);
		}

		/**
		 * Returns a builder for the decompression algorithm. This sets the default to
		 * an otherwise blank template. Any arguments given with
		 * {@link CompressionBuilder#args(String... args)} will not overwrite the
		 * default argument.
		 * 
		 * @param prog an {@link Algorithm} to be used in decompression
		 * @return this
		 */
		public static CompressionBuilder decompress(Algorithm prog){
			return new CompressionBuilder(prog).setDefaultDecompression();
		}

		/**
		 * De-selects the default compression switch (-d). Add custom args with
		 * {@link CompressionBuilder#args(String... args)}
		 * 
		 * For compression this has no effect.
		 * 
		 * @return this
		 */
		public CompressionBuilder noDefaultDecompression(){
			defaultDecompressArgs = new String[0];
			return this;

		}

		/**
		 * Sets the maximum size for the compressed/decompressed data. Maximum value is
		 * {@link Integer#MAX_VALUE } - 8 which is the biggest array size for some
		 * virtual machines. Negative values are treated as max value.
		 * 
		 * @param length maximum size for the input data.
		 * @return this
		 */
		public CompressionBuilder maxLength(int length){
			this.maxSize = length < ARRAY_MAX_SIZE ? length : ARRAY_MAX_SIZE;
			return this;
		}

		/**
		 * Give custom arguments for the compression algorithm. Arguments must be valid
		 * for the selected program and they are not checked in any way.
		 * 
		 * @param args Custom arguments for the subprocess
		 * @return this
		 */
		public CompressionBuilder args(String... args){
			this.args = args;
			return this;
		}

		/**
		 * Creates a {@link Compressor} object with given parameters.
		 * 
		 * @return A {@link Compressor}
		 * @throws IOException          if the subprogram cannot be created
		 * @throws InterruptedException if waiting for subprogram to start is
		 *                              interrupted.
		 */
		public Compressor build() throws IOException, InterruptedException{
			Compressor c = new Compressor(prog, defaultDecompressArgs, args);
			c.setMaxSize(maxSize >= 0 ? maxSize : ARRAY_MAX_SIZE);
			return c;

		}

		private CompressionBuilder setDefaultDecompression(){
			defaultDecompressArgs = prog.decompressOptions;
			return this;
		}

		private CompressionBuilder(Algorithm prog){
			this.prog = prog;

		}
	}
}
