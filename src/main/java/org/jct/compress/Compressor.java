package org.jct.compress;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class can be used to compress arbitrary bytes by using external
 * compression programs. The class cannot be initialized by itself, use
 * {@link CompressionBuilder} instead.
 * 
 *
 */
public class Compressor extends OutputStream {

    /*
     * This lock is claimed by the CompressorInputReader when it is started as a
     * thread. This makes sure that the reading of subprocess input is finished
     * before the data can be accessed.
     */
    private ReentrantLock processWait = new ReentrantLock();

    /*
     * Used to halt the constructor of the Compressor, until the
     * CompressorInputReader is ready and has picked up the Lock
     */
    private CountDownLatch processStart = new CountDownLatch(1);

    /*
     * Subprocess, the actual data compression program
     */
    private Process proc;

    /*
     * InputStream to read the output of the process
     */
    private InputStream processOutput;

    /*
     * The outputstream that is connected to process input, to put the data into.
     */
    private OutputStream processInput;

    /*
     * The reader for the process output. Reads and collects it to an InputStream.
     */
    private CompressorInputReader in;

    private Thread readerThread;

    /**
     * Max size for the local data TODO: should this be given for inputreader, not
     * here!
     */
    private int maxSize;

    private File diskCacheDirectory;

    protected Compressor(Algorithm program, boolean keepfile, boolean autoread, String[]... args)
	    throws IOException, InterruptedException {

	ProcessBuilder pb = new ProcessBuilder();
	ArrayList<String> arguments = new ArrayList<>();
	arguments.add(program.programName);
	for (String[] a : args) {
	    arguments.addAll(Arrays.asList(a));
	}
	proc = pb.command(arguments).start();
	processOutput = new BufferedInputStream(proc.getInputStream());
	processInput = proc.getOutputStream();

	in = new CompressorInputReader(processOutput, keepfile);

	if (autoread) {
	    readerThread = new Thread(in, "jCompress input reader");
	    readerThread.start();
	    processStart.await();
	}

    }

    /**
     * Reads the contents of the file into an array. Fails if the file is larger
     * than array can take. This method blocks until the compressor program is done.
     * 
     * @return a byte array that contains the compressed/decompressed data
     * @throws IOException
     * 
     * 
     */
    public byte[] getBytes() throws IOException {
	return in.readFully();
    }

    /**
     * Returns an inputstream to read the compressed data. This method blocks until
     * the compressor program is done.
     * 
     * @return an InputStream that contains the compressed/decompressed data
     * @throws IOException
     */
    public InputStream getPreloadedInputStream() throws IOException {
	return in.getInputStream();
    }

    /**
     * Returns the raw inputstream
     * 
     * @return an InputStream that contains the compressed/decompressed data
     * @throws IOException
     */
    public InputStream getRawInputStream() throws IOException {
	return processOutput;
    }

    /**
     * Returns the size of the compressed data.
     * 
     * @return size of the compressed data
     */
    public long getCompressedLength() {
	return in.getTotalRead();
    }

    /**
     * Write more bytes to the subprocess
     * 
     * @param bytes a byte array that will be written to the compression program
     * @throws IOException if there is any I/O error while writing data to the
     *                     subprocess
     */
    @Override
    public void write(byte[] bytes) throws IOException {
	processInput.write(bytes);
    }

    @Override
    public void write(byte[] bytes, int off, int len) throws IOException {
	processInput.write(bytes, off, len);
    }

    @Override
    public void write(int b) throws IOException {
	processInput.write(b);
    }

    /**
     * Finished compressing. After this, no more bytes can be written. This must be
     * done to guarantee that the subprocess will eventually finish.
     * 
     * @throws IOException if there is any I/O error while writing data to the
     *                     subprocess
     */
    @Override
    public void close() throws IOException {
	processInput.flush();
	processInput.close();

    }

    public void complete() throws InterruptedException, IOException {
	try {
	    close();
	} catch (IOException e) {
	    // TODO: cache this
	}
	readerThread.join();
	if (in.cached != null) {
	    throw new IOException(in.cached);
	}
    }

    /**
     * 
     * A runnable that will read the subprocess stream asynchronously.
     *
     */
    private class CompressorInputReader implements Runnable {

	/**
	 * If {@code Runnable#run()} fails, the exception is cached here for later use
	 */
	private IOException cached;

	/*
	 * The actual InputStream that will be read
	 */
	private InputStream subProcessIn;

	/*
	 * The output where results will be written
	 */
	private OutputStream out = new ByteArrayOutputStream();

	/*
	 * If the file should be deleted after reading
	 */
	private boolean keepfile;

	/*
	 * Marks if a file has been used.
	 */
	private boolean fileUsed;

	/*
	 * The temp while, where data might be written, if the max size is reached.
	 */
	private File tmp;

	/*
	 * How many bytes have been read from the subprocess
	 */
	private long subProcessRead;

	/**
	 * Creates a CompressorInputReader for given stream. The only user will be the
	 * enclosing class.
	 * 
	 * @param in
	 */
	private CompressorInputReader(InputStream in, boolean keepFile) {
	    this.subProcessIn = in;
	    this.keepfile = keepFile;
	}

	/**
	 * Returns how many bytes have been read back from the subprocess. The
	 * subprocess might not write anything before it has read the input fully.
	 * 
	 * @return
	 */
	public long getTotalRead() {
	    return subProcessRead;
	}

	/**
	 * Reads the InputStream completely. If the InputStream is still a
	 * ByteArrayOutputStream, the contents of that are returned. Otherwise the
	 * actual InputStream will be read.
	 * 
	 * @return
	 * @throws IOException
	 */
	public byte[] readFully() throws IOException {

	    processWait.lock();
	    if (out instanceof ByteArrayOutputStream) {
		return ((ByteArrayOutputStream) out).toByteArray();
	    }

	    try (ByteArrayOutputStream b = new ByteArrayOutputStream()) {

		InputStream is = getInputStream();

		int read;
		byte[] buffer = new byte[1024];

		while ((read = is.read(buffer)) != -1) {
		    b.write(buffer, 0, read);
		}

		processOutput.close();
		if (fileUsed && !keepfile) {
		    tmp.delete();
		}
		return b.toByteArray();

	    } finally {
		processWait.unlock();
	    }

	}

	/**
	 * Returns the InputStream for the data that was read from the subprocess.
	 * 
	 * @return the InputStream that contains the read data.
	 */
	public InputStream getInputStream() throws IOException {
	    try {// NOSONAR, it triggers because we return an autocloseable
		processWait.lock();
		if (fileUsed) {
		    if (keepfile) {
			return new FileInputStream(tmp);
		    }
		    return new FileDeletingInputStream(tmp);
		}
		return new ByteArrayInputStream(((ByteArrayOutputStream) out).toByteArray());
	    } finally {
		processWait.unlock();
	    }
	}

	@Override
	public void run() {
	    try {
		processWait.lock();
		processStart.countDown();
		byte[] buffer = new byte[1024];
		subProcessRead = 0;
		int read = 0;
		try {
		    while ((read = subProcessIn.read(buffer)) != -1) {
			checkOutputCapacity(subProcessRead + read);
			out.write(buffer, 0, read);
			subProcessRead += read;
		    }
		    processOutput.close();
		} catch (IOException ex) {
		    // cache exception in order to throw it later
		    this.cached = ex;
		}
	    } finally {
		processWait.unlock();
	    }
	}

	/**
	 * If the max size of the output is reached, contents are dumped into an empty
	 * file.
	 * 
	 * @param total
	 * @throws IOException
	 */
	private void checkOutputCapacity(long total) throws IOException {
	    if (maxSize < 0) {
		return;
	    }
	    if (!fileUsed && total > maxSize) {
		File f = File.createTempFile("java-compression.", ".tmp", diskCacheDirectory);
		setFile(f);
	    }
	}

	private void setFile(File file) throws IOException {
	    if (!fileUsed) {
		this.tmp = file;
		this.fileUsed = true;
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(((ByteArrayOutputStream) out).toByteArray());
		out = fos;
	    }
	}
    }

    /**
     * A FileInputStream, that deletes the file after reading. Be careful where to
     * use. Because temp files might be created a lot, they could clutter the temp
     * folder. This makes sure the file is always deleted after reading.
     *
     */
    private class FileDeletingInputStream extends FileInputStream {

	private File tempfile;

	public FileDeletingInputStream(File file) throws FileNotFoundException {
	    super(file);
	    this.tempfile = file;
	}

	@Override
	public void close() throws IOException {
	    super.close();
	    Files.delete(tempfile.toPath());
	}
    }

    /**
     * 
     * Used to build the compressor/decompressor object.
     *
     */
    public static class CompressionBuilder {

	/**
	 * The maximum size of array to allocate. Some VMs reserve some header words in
	 * an array. Attempts to allocate larger arrays may result in OutOfMemoryError:
	 * Requested array size exceeds VM limit
	 * 
	 * Integer.MAX_VALUE - 8 should be quite fine.
	 */
	private static final int ARRAY_MAX_SIZE = Integer.MAX_VALUE - 8;

	private String[] defaultDecompressArgs = new String[0];
	private String[] args = new String[0];
	private int maxSize = ARRAY_MAX_SIZE;
	private Algorithm prog;
	private File file;
	private File tempdir;

	private boolean directToFile;

	private boolean keepfile;

	/**
	 * Should the compressor start the input reader thread.
	 */
	private boolean autoread;

	/**
	 * Returns a builder for the compression algorithm. This is a blank template and
	 * can be customized with any way by giving arguments with
	 * {@link CompressionBuilder#args(String... args)}
	 * 
	 * @param prog an {@link Algorithm} to be used in compression
	 * @return this
	 */
	public static CompressionBuilder compress(Algorithm prog) {
	    return new CompressionBuilder(prog);
	}

	/**
	 * Returns a builder for the decompression algorithm. This sets the default to
	 * an otherwise blank template. Any arguments given with
	 * {@link CompressionBuilder#args(String... args)} will not overwrite the
	 * default argument.
	 * 
	 * @param prog an {@link Algorithm} to be used in decompression
	 * @return this
	 */
	public static CompressionBuilder decompress(Algorithm prog) {
	    return new CompressionBuilder(prog).setDefaultDecompression();
	}

	/**
	 * Deselects the default compression switch (-d). Add custom args with
	 * {@link CompressionBuilder#args(String... args)}
	 * 
	 * For compression this has no effect.
	 * 
	 * @return this
	 */
	public CompressionBuilder noDefaultDecompression() {
	    defaultDecompressArgs = new String[0];
	    return this;

	}

	/**
	 * Sets the maximum size for the compressed/decompressed data. Maximum value is
	 * {@link Integer#MAX_VALUE } - 8 which is the biggest array size for some
	 * virtual machines. Negative values are treated as max value.
	 * 
	 * @param length maximum size for the input data.
	 * @return this
	 */
	public CompressionBuilder maxLength(int length) {
	    if (length < 0) {
		throw new IllegalArgumentException("Length must be positive, not " + length);
	    }
	    this.maxSize = length <= ARRAY_MAX_SIZE ? length : ARRAY_MAX_SIZE;
	    return this;
	}

	/**
	 * Give custom arguments for the compression algorithm. Arguments must be valid
	 * for the selected program and they are not checked in any way.
	 * 
	 * @param args Custom arguments for the subprocess
	 * @return this
	 */
	public CompressionBuilder args(String... args) {
	    this.args = args;
	    return this;
	}

	/**
	 * Determines that the output should be written directly to a file.
	 */
	public CompressionBuilder directToFile(File file) {
	    this.file = file;
	    this.directToFile = true;
	    this.keepfile = true;
	    return this;
	}

	/**
	 * Defines a maximum size for read data that remains in the memory. If more
	 * bytes are read, a temporary file is created.
	 * 
	 * @param size how many bytes can be read before using a temp file
	 */
	public void setMaxSize(int size) {
	    if (size < 0) {
		throw new IllegalArgumentException("Size must be positive, not " + size);
	    }
	    this.maxSize = size;
	}

	/**
	 * Creates a {@link Compressor} object with given parameters.
	 * 
	 * @return A {@link Compressor}
	 * @throws IOException          if the subprogram cannot be created
	 * @throws InterruptedException if waiting for subprogram to start is
	 *                              interrupted.
	 */
	public Compressor build() throws IOException, InterruptedException {
	    Compressor c = new Compressor(prog, keepfile, autoread, defaultDecompressArgs, args);// NOSONAR
	    setMaxSize(maxSize >= 0 ? maxSize : ARRAY_MAX_SIZE);
	    if (file != null) {
		setFile(file);
	    }
	    if (directToFile) {
		setMaxSize(0);
	    }
	    return c;

	}

	private CompressionBuilder setDefaultDecompression() {
	    defaultDecompressArgs = prog.getDecompressOptions();
	    return this;
	}

	private CompressionBuilder autoRead(boolean autoread) {
	    this.autoread = autoread;
	    return this;
	}

	private CompressionBuilder(Algorithm prog) {
	    this.prog = prog;

	}

	private void setFile(File file) throws IOException {
	    setFile(file);
	}

	private void setDirectory(File tempdir) {
	    this.tempdir = tempdir;
	}
    }

}
