package org.jct.compress;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class CompressedFileWriter extends OutputStream implements AutoCloseable {

	CompressorProcess cp;

	@Override
	public void write(int b) throws IOException{
		cp.toProcess.write(b);
	}

	public CompressedFileWriter(File target, Algorithm program, String[]... args)
			throws IOException{
		ProcessBuilder pb = new ProcessBuilder();
		ArrayList<String> arguments = new ArrayList<>();
		arguments.add(program.programName);
		for(String[] a : args){
			arguments.addAll(Arrays.asList(a));
		}
		pb.command(arguments).redirectOutput(target);
		// System.out.println(arguments);
		cp = new CompressorProcess(pb, true);
//		System.out.println("<init> CompressedFileWriter() with " + arguments);
	}

	public CompressedFileWriter(File target, Algorithm program, String... args) throws IOException{
		this(target, program, args, new String[0]);
	}

	@Override
	public void close() throws IOException{
		cp.closeIn();
		cp.closeOut();
		cp.waitTermination();

	}
}
