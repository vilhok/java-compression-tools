package org.jct.compress;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

// TODO: this one needs better args, namely, the processbuilder.
public class CompressorProcess {

	/*
	 * Subprocess, the actual data compression program
	 */
	private Process proc;

	/*
	 * InputStream to read the output of the process
	 */
	protected InputStream fromProcess;

	/*
	 * InputStream to read the output of the process
	 */
	protected InputStream procErr;;

	/*
	 * The outputstream that is connected to process input, to put the data
	 * into.
	 */
	protected OutputStream toProcess;

	public CompressorProcess(ProcessBuilder pb, boolean startProcSTDIN) throws IOException{
		proc = pb.start();
		fromProcess = new BufferedInputStream(proc.getInputStream());

		procErr = new BufferedInputStream(proc.getErrorStream());
		if(startProcSTDIN)
			toProcess = proc.getOutputStream();
	}

	public CompressorProcess(Algorithm prog, String[]... args) throws IOException{
		ProcessBuilder pb = new ProcessBuilder();
		ArrayList<String> arguments = new ArrayList<>();
		arguments.add(prog.programName);
		for(String[] a : args){
			arguments.addAll(Arrays.asList(a));
		}
		proc = pb.command(arguments).start();
		fromProcess = new BufferedInputStream(proc.getInputStream());
		toProcess = proc.getOutputStream();
	}

	public CompressorProcess(File targetFile, Algorithm prog, String[]... args) throws IOException{
		ProcessBuilder pb = new ProcessBuilder().redirectOutput(targetFile);
		ArrayList<String> arguments = new ArrayList<>();
		arguments.add(prog.programName);
		for(String[] a : args){
			arguments.addAll(Arrays.asList(a));
		}
		proc = pb.command(arguments).start();
		fromProcess = new BufferedInputStream(proc.getInputStream());
		toProcess = proc.getOutputStream();
	}

	public int read(byte[] bytes) throws IOException{
		return fromProcess.read(bytes);
	}

	public void write(byte[] bytes) throws IOException{
		toProcess.write(bytes);
	}

	public void write(int b) throws IOException{
		toProcess.write(b);
	}

	public void flush() throws IOException{
		toProcess.flush();
	}

	public void closeOut() throws IOException{
		if(toProcess != null){
			flush();
			toProcess.close();
		}
	}

	public void closeIn() throws IOException{
		fromProcess.close();
	}

	public String readerr() throws IOException{
		return new String(procErr.readAllBytes());
	}

	public void waitTermination() throws IOException{
		try{
			proc.waitFor();
		}catch(InterruptedException e){
			throw new IOException(e);
		}
	}
}
