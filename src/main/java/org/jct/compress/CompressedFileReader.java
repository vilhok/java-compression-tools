package org.jct.compress;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class CompressedFileReader extends InputStream {

	CompressorProcess subprocess;

	public CompressedFileReader(File target, Algorithm program) throws IOException{
		this(target, program, new String[0]);
	}

	public CompressedFileReader(File sourceFile, Algorithm program, String[]... args)
			throws IOException{
		ProcessBuilder pb = new ProcessBuilder();
		ArrayList<String> arguments = new ArrayList<>();
		arguments.add(program.programName);
		for(String[] a : args){
			arguments.addAll(Arrays.asList(a));
		}

		arguments.add(sourceFile.getAbsolutePath());
		pb.command(arguments);

		subprocess = new CompressorProcess(pb, false);
		System.out.println("<init> CompressedFileReader() with " + arguments);
	}

	public CompressedFileReader(File target, Algorithm program, String... args) throws IOException{
		this(target, program, args, new String[0]);
	}

	@Override
	public void close() throws IOException{
		subprocess.closeIn();
		subprocess.closeOut();
		subprocess.waitTermination();
	}

	@Override
	public int read() throws IOException{
		// System.out.println("read");
		int asd = subprocess.fromProcess.read();
		// System.out.println(asd);
		return asd;
	}

	public String getErr() throws IOException{
		return subprocess.readerr();
	}
}
