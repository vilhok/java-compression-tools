package org.jct.compress;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * An Algorithm represents and external program that can be used for
 * compressing/decompressing data.
 */
public enum Algorithm {
	GZIP("gzip", "gz", new String[]{ "-d", "--stdout" }),
	XZ("xz", "xz", new String[]{ "-d", "--stdout" }),
	ZSTD("zstd", "zst", new String[]{ "-d", "--stdout" });

	/**
	 * The name of the binary that will be called when this Algorithm is used
	 */
	public final String programName;

	/**
	 * Default file extension
	 */
	public final String extension;

	/**
	 * Default de-compressing switches that will be used when doing
	 * decompression.
	 * Default de-compressing switches that will be used when doing de-compression
	 */
	private final String[] decompressOptions;

	private Algorithm(String program, String extension, String[] decompressOptions){
		this.programName = program;
		this.extension = extension;
		this.decompressOptions = decompressOptions;
	}

	public String[] getDecompressOptions(){
		// to preserve the immutability of the original array
		return Arrays.copyOf(decompressOptions, decompressOptions.length);

	}

	/*
	 * This stores the available algorithms. This is initialized when the
	 * getAvailable() is called first time and remains until it is forced to be
	 * checked again.
	 */
	private static Algorithm[] available;

	/**
	 * Returns the compression algorithms that are available. This calls each
	 * program in {@code Algorithm.values()} to determine if the exist in the
	 * PATH.
	 * 
	 * The checking is only done the first time, unless forceReload parameter is
	 * true. This will perform the check again.
	 * 
	 * @param forceReload if true, available programs are checked again.
	 * @return the available compression programs as an {@link Algorithm} -array
	 */
	public static Algorithm[] getAvailable(boolean forceReload){
		if(!forceReload && available != null){
			return available;
		}
		ArrayList<Algorithm> found = new ArrayList<>();

		for(Algorithm k : Algorithm.values()){
			Process p = null;
			try{
				p = new ProcessBuilder(k.programName).start();
			}catch(IOException e){
				// A failing process is not relevant, that simply marks he
				// algorithm is not available
			}finally{
				if(p != null){
					p.destroy();
				}
			}
			found.add(k);
		}
		available = found.toArray(new Algorithm[0]);
		return available;
	}

	/**
	 * 
	 * Returns the compression algorithms that are available. This calls each
	 * program in {@code Algorithm.values()} to determine if the exist in the
	 * PATH.
	 *
	 * @return the available compression programs as an {@link Algorithm} -array
	 */
	public static Algorithm[] getAvailable(){
		return getAvailable(false);
	}

}
