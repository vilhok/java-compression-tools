package org.jct.compressiontest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.jct.compress.Algorithm;
import org.jct.compress.Compressor;
import org.junit.jupiter.api.Test;

class CompressionDecompressionTest {

	@Test
	public void testDecompressCompress() {
		// build
		try {
			Compressor c = Compressor.Builder //
					.compress(Algorithm.ZSTD) //
					.args(new String[] { "--ultra", "-22", "-T0" }) //
					.build();

			byte[] bytes = Files.readAllBytes(Paths.get("lipsum.txt"));
			c.write(bytes);
			c.finish();

			byte[] compressed = c.getBytes();

			System.out.println(compressed.length);
			Compressor d = Compressor.Builder //
					.decompress(Algorithm.ZSTD) //
					.build();

			d.write(compressed);
			d.finish();

			byte[] decompressed = d.getBytes();

			System.out.println(decompressed.length);
			assertTrue(byteEquals(bytes, decompressed));

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean byteEquals(byte[] bytes, byte[] bytes2) {
		if (bytes.length != bytes2.length) {
			return false;
		}
		for (int i = 0; i < bytes.length; i++) {
			if (bytes[i] != bytes2[i]) {
				return false;
			}
		}
		return true;
	}
}
