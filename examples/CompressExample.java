package compress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import compress.Compressor.CompressionBuilder;

public class CompressExample {

	public static void main(String[] args) throws IOException, InterruptedException{

		// get available compression programs
		Algorithm[] programs = CompressionBuilder.getAvailable();

		// Get some data to compress
		byte[] bytes = Files.readAllBytes(Paths.get("lipsum.txt"));

		for (Algorithm p : programs){
			Compressor c = new Compressor(p, new String[]{});

			c.write(bytes);
			c.finish();
			byte[] result = c.getBytes();

			FileOutputStream fos = new FileOutputStream(new File("lipsum.txt" + p.extension));
			fos.write(result);
			fos.close();
			System.out.println("done");
		}
	}

	public static void buildDefault() throws IOException, InterruptedException{

		Compressor c = CompressionBuilder //
				.compress(Algorithm.ZSTD) //
				.build();
	}

	public static void customArgs() throws IOException, InterruptedException{

		Compressor c = CompressionBuilder //
				.compress(Algorithm.ZSTD) //
				.args("--ultra", "-22") //
				.build();
	}

	public static void findProgramsExample(){
		// get available compression algorithms
		Algorithm[] programs = CompressionBuilder.getAvailable();
	}

}
