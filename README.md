# Java compression tools

Java has built-in library to handle gzip and zip. Zip is a pain in general and for gzip you need to extend `GzipOutputStream` in order to access the `Deflater` to set the compression level. Also it seems to very slow (twice as slow for single 800 MB file).


This library provides an interface for using external compression programs efficiently within Java code. This can be used to compress arbitrary bytes by calling external programs. The process is transparent and the user does not need to manage the subprocesses or files. The compressed data is easily accessed as arrays or streams.

## Prerequisites
Currently following programs are supported:

* gzip 
* xz
* zstd

You need to have such programs installed and in your PATH.

# Checking for available programs
In order to check which programs can be used, you can do following:

```java
Algorithm[] programs = Algorithm.getAvailable();
```

This checks which programs are installed by trying to start them. Subsequent calls just read the previous result, unless optional `true` is given as argument.

#### Compressing data
Start by creating compressor with given `Algorithm` by using the `CompressionBuilder` -class:

```java
Compressor c = CompressionBuilder 
		.compress(Algorithm.ZSTD) 
		.build();

// example data
byte[] bytes = Files.readAllBytes(Paths.get("lipsum.txt"));
c.write(bytes);
c.finish();  // closes the output to finalize the compression
```

Simple as that. To access the compressed data, you have two options:

```java
//get all the bytes
byte[] compressed = c.getBytes();

//or an InputStream
InputStream compressed = c.getInputStream();
```

First one is more convenient for short data. Internally the compressed data is always read to a byte array, but if given threshold is exceeded the data is written to a temporary file.

Either method can be used as `getBytes()` uses `getInputStream()` internally. If you want to make sure that the data fits to an array after compression, you can check it easily:

```java
long length = c.getCompressedLength();
```

In case you want to limit memory usage beforehand you can manually set the max size for the array:

```java
Compressor c = CompressionBuilder 
		.compress(Algorithm.ZSTD) 
		.maxLength(8192)  //set the max compressed/uncompressed size
		.build();
```
If this is exceeded, the data is automatically written to disk instead. 

#### Custom parameters

You can also you custom parameters, as long as the program understands them:

```java
Compressor c = CompressionBuilder 
		.compress(Algorithm.ZSTD)
		.args("--ultra", "-22", "-T0") // custom args (multiple Strings or String[])
		.maxLength(8192)  
		.build();
```

#### Decompressing data

Decompression is done the same way. So far the program does not check magic numbers or anything, so you must know which program to use:

```java
Compressor c = CompressionBuilder 
		.decompress(Algorithm.ZSTD)
		.build();
```

decompress just sets the default decompression switch (usually -d). You can omit this by calling `noDefaultDecompression()`, but then you need to set the args manually, or the program will compress instead of decompress.

